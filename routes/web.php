<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@home');
// Route::get('/register', 'AuthController@register');
// Route::get('/welcome', 'AuthController@welcome');

// CRUD Layout
Route::get('/', function () {
    return view('layout/dashboard');
});
Route::get('/table', 'TableController@table');
Route::get('/data-table', 'TableController@datatable');

// CRUD Cast
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');


// CRUD Genre
// Route::get('/genre', 'GenreController@index');
// Route::get('/genre/create', 'GenreController@create');
// Route::post('/genre', 'GenreController@store');
// Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
// Route::put('/genre/{genre_id}', 'GenreController@update');
// Route::delete('/genre/{genre_id}', 'GenreController@destroy');
Route::resource('genre','GenreController');

// CRUD Film
    //cek route php artisan route:list
Route::resource('film', 'FilmController');

