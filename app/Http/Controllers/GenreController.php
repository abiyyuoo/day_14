<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);
        $query = DB::table('genre')->insert([
            "nama" => $request["nama"],
        ]);
        return redirect('/genre')->with(['success' => 'Data Berhasil Di Tambah']);
    }

    public function index()
    {
        $genre = DB::table('genre')->get();
        return view('genre.index', compact('genre'));
    }

    // public function show($id)
    // {
    //     $genre = DB::table('genre')->where('id', $id)->first();
    //     return view('genre.show', compact('genre'));
    // }

    public function edit($id)
    {
        $genre = DB::table('genre')->where('id', $id)->first();
        return view('genre.edit', compact('genre'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $query = DB::table('genre')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"]
            ]);
        return redirect('/genre')->with(['success' => 'Data Berhasil Di Update']);
    }

    public function destroy($id)
    {
        $query = DB::table('genre')->where('id', $id)->delete();
        return redirect('/genre')->with(['success' => 'Data Dihapus']);
    }
}
