<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Blank Page</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href=" {{asset('/adminlte/plugins/fontawesome-free/css/all.min.css')}} ">
  <!-- Theme style -->
  <link rel="stylesheet" href=" {{asset('/adminlte/dist/css/adminlte.min.css')}} ">
  <!-- datatables -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  @include('layout.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @yield('dashboard')
    @yield('content')
    @yield('datatable')
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {{-- @include('footer') --}}

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src=" {{asset('/adminlte/plugins/jquery/jquery.min.js')}} "></script>
<!-- Bootstrap 4 -->
<script src=" {{asset('/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}} "></script>
<!-- AdminLTE App -->
<script src=" {{asset('/adminlte/dist/js/adminlte.min.js')}} "></script>
<!-- AdminLTE for demo purposes -->
<script src=" {{asset('/adminlte/dist/js/demo.js')}} "></script>

<!-- datatables -->
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src=" {{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}} "></script>

<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
</body>
</html>
