@extends('layout/home')
@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Film {{$film->judul}} </h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
  <div class="card">
      <div class="col-sm-4">
        <img src=" {{asset('poster/' . $film->poster)}} " class="card-img-top" alt="">
      </div>
      <div class="row">
        
        <div class="col-14">
            <div class="card-body">
                <h4> {{$film->judul}}  {{$film->tahun}} </h4>
                <p class="card-text" align="justify"> {{$film->ringkasan}} </p>
                <a href="/film" class="btn btn-primary" style="text-align: justify">Back</a>
              </div>
        </div>
    </div>
  
    <!-- /.card-body -->
  </div>
</section>
    
@endsection