@extends('layout/home')
@section('content')

<section class="content">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">DataTable with default features</h3>
      <a href="/film/create"><button class="btn btn-primary btn-sm float-right mr-6" type="button"><i class="fas fa-plus" ></i> Tambah Data</button></a>
    </div>
    @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>    
          <strong>{{ $message }}</strong>
      </div>
    @endif
  
    <div class="row">
      @foreach ($film as $item)
      <div class="col-2">
        <div class="card">
          <img src=" {{asset('poster/' . $item->poster)}} " class="card-img-top" alt="">
          <div class="card-body">
            <h4> {{$item->judul}}  {{$item->tahun}} </h4>
            <p class="card-text"> {{Str::limit($item->ringkasan,30)}} </p>
            <a href="/film/{{$item->id}}" class="btn btn-info" >Read More</a>
          </div>
        </div>
      </div>
          
      @endforeach
    </div>
  
    <!-- /.card-body -->
  </div>
</section>
    
@endsection