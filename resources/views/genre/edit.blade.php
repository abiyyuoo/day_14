@extends('layout.home');

@section('content')

<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Edit Genre</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Edit Genre</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            <h2>Edit Genre {{$genre->id}}</h2>
            <form action="/genre/{{$genre->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{$genre->nama}}" id="nama" placeholder="Masukkan Nama">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
          
            
        <!-- /.card-body -->
      </div>
    
@endsection
